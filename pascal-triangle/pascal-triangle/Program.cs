﻿using System;

namespace pascal_triangle
{
    class PascalTriangle
    {
        static void Main(string[] args)
        {
            // Get order for triangle
            Console.WriteLine("Enter Pascal's Triangle order ");
            int order = Convert.ToInt32(Console.ReadLine());
            int coef = 1, space, i, j;
            for (i = 0; i < order; i++)
            {
                for (space = 1; space <= order - i; space++)
                    Console.Write("  ");

                // printing values of the triangle
                for (j = 0; j <= i; j++)
                {
                    if (j == 0 || i == 0)
                        coef = 1;
                    else
                        coef = coef * (i - j + 1) / j;
                    Console.Write("   " + coef);
                }
                Console.WriteLine();
            }
        }
    }
}
